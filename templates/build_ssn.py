#!/usr/bin/python

import networkx
from Bio import SeqIO

G = networkx.Graph()
nodes = set()

for record in SeqIO.parse("$infile", 'fasta'):
	nodes.add( record.id )

nodes = list(nodes)
G.add_nodes_from(nodes)

with open("$identities", 'r') as f:
	for line in f:
		a, b, identity = line.strip().split()
		if $selfloops == False:
			if a == b:
				continue

		if float(identity) >= ${params.threshold}:
			G.add_edge(a,b, identity=float(identity))

networkx.write_gml(G, 'network.gml')

