#!/usr/bin/env nextflow

params.infile = ''
params.threshold = 0.4
params.selfloops = false

if (params.selfloops == false) {
	selfloops = 'False'
} else if (params.selfloops == true) {
	selfloops = 'True'
}	

infile = file(params.infile)

process copyData {
	output:
	file "infile.fasta" into run_nw

	script:
	"""
	cp ${infile} "infile.fasta"
	"""
}


process runNeedleall {
	container 'ravenlocke/emboss:6.6.0.0'

	input:
	file infile from run_nw

	output:
	file "identities.txt" into build_ssn

	script:
	template "run_nw.py"
}

process buildSSN {
	container 'ravenlocke/scientific-python:latest'
	
	input:
	file identities from build_ssn
	file infile from run_nw

	script:
	template "build_ssn.py"
}
